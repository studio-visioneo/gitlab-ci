#!/bin/sh

# Get the wordpress directory
WP_DIR="/app/wordpress"

# Get the config directory
CONFIG_DIR="/app/config/wp-config"

# Get all files in the config directory
FILES=$(find "$CONFIG_DIR" -type f)

# For each file
for FILE in $FILES
do
    # Get the relative path of the file
    RELATIVE_PATH=$(echo "$FILE" | sed "s|$CONFIG_DIR||")

    # Create destination directory if it doesn't exist
    mkdir -p "$WP_DIR$(dirname $RELATIVE_PATH)"

    # Copy the file to the wordpress directory
    cp "$FILE" "$WP_DIR$RELATIVE_PATH"
done